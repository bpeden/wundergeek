#!/usr/bin/python

import requests

# Your own API key.  You can obtain and API key by signing up for one at
# http://www.wunderground.com/weather/api/
api_key = '7a7bb6ff632c375c'

# The ID of the weather station we're interested in.
pws_id = 'KGAATLAN88'


url = "http://api.wunderground.com/api/{0}/conditions/pws:1/q/pws:{1}.json".format(api_key, pws_id)
try:
	r = requests.get(url)
except:
	# exit silently on error
	exit(1)

parsed_json = r.json()
print(parsed_json)


#uncomment the following line to see what other fileds are abvailable to you.
#print json_string

#Edit these lines to extract the information you're interested in.
weather = parsed_json['current_observation']['weather']
temp_f = parsed_json['current_observation']['temp_f']

#Edit this line to change the way that the extracted data is displayed.
print "Current conditions at pws {0} are:\nWeather: {1}\nTempaerature: {2}".format(pws_id, weather, temp_f)
